package com.qxjc.manage.dao;

import java.util.List;

import com.qxjc.manage.pojo.ParkApplyInfo;

public interface ParkApplyInfoMapper {
	
    int deleteByPrimaryKey(String id);

    int insert(ParkApplyInfo record);

    int insertSelective(ParkApplyInfo record);

    ParkApplyInfo selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ParkApplyInfo record);

    int updateByPrimaryKey(ParkApplyInfo record);
    
    List<ParkApplyInfo> selectByDeptId(String deptId);
    
    List<ParkApplyInfo> selectByUserId(String userId);
}