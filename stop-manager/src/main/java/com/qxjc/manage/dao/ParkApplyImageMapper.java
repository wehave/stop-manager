package com.qxjc.manage.dao;

import com.qxjc.manage.pojo.ParkApplyImage;

public interface ParkApplyImageMapper {
	
    int deleteByPrimaryKey(String id);

    int insert(ParkApplyImage record);

    int insertSelective(ParkApplyImage record);

    ParkApplyImage selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ParkApplyImage record);

    int updateByPrimaryKeyWithBLOBs(ParkApplyImage record);
    
}