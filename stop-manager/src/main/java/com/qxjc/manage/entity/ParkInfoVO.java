package com.qxjc.manage.entity;

import java.io.File;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ParkInfoVO {
	
	private String infoId;
	
	private String imageId;

    /** 图像id */
    private String applyImgId;
    
    /** 用户id */
    private String userId;

    /** 部门id  */
    private String deptId;

    /** 业务类型 1:新增   2:年审   */
    private Integer applyType;

    /** 名称 */
    private String name;

    /** 地址 */
    private String address;

    /** 负责人 */
    private String header;

    /** 负责人电话 */
    private String headerPhone;

    /** 法人 */
    private String boss;

    /** 法人电话 */
    private String bossPhone;

    /** 停车场类型 1:公用 2:私用 */
    private Integer type;

    /** 总数 */
    private Integer parkSum;

    /** 总面积 */
    private Float parkArea;

    /** 地下 */
    private Integer underground = 0;

    /** 地上 */
    private Integer ground = 0;

    /** 院内 */
    private Integer yard = 0;

    /** 机械 */
    private Integer machine = 0;

    /** 创建时间 */
    private Date createTime;

    /** 备注 */
    private String remark;
    
    /** 状态 0:开始 1:审核 2:完成 3:删除  */
    private Integer state;
    
    /** 修改状态 0:可修改 1:不可修改  */
    private Integer  modified;
    
    
    
	/** 营业执照 */
	private MultipartFile yyzz;

	/** 税务登记证 */
	private MultipartFile swdjz;

	/** 法人身份证 */
	private MultipartFile frsfzj;

	/** 经办人身份证 */
	private MultipartFile jbrsfzj;

	/** 土地证 */
	private MultipartFile tdz;

	/** 土地租赁合同1 */
	private MultipartFile tdzlht1;

	/** 土地租赁合同2 */
	private MultipartFile tdzlht2;

	/** 土地租赁合同3 */
	private MultipartFile tdzlht3;

	/** 平面图 */
	private MultipartFile pmt;

	/** 设施设备清单 */
	private MultipartFile sbqd;

	/** 收费标准 */
	private MultipartFile sfbz;

	/** 停放制度 */
	private MultipartFile tfzd;

	/** 安全制度 */
	private MultipartFile aqzd;

	/** 防汛制度 */
	private MultipartFile fxzd;

	/** 消防管理制度 */
	private MultipartFile xfzd;
	
	/** 智慧信息平台  */
	private MultipartFile zhpt;


}
