package com.qxjc.manage.entity;

import java.io.File;

import groovy.transform.ToString;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ToString
public class WorkflowParam {
	
	/** 流程定义部署文件 */
	private File file;		
	
	/** 流程定义名称 */
	private String filename;
	
	/** 申请单ID */
	private String id;
	
	/** 部署对象ID */
	private String deploymentId;
	
	/** 资源文件名称 */
	private String imageName;	
	
	/** 任务ID */
	private String taskId;	
	
	/** 连线名称 */
	private String outcome;		
	
	/** 备注 */
	private String comment;

}
