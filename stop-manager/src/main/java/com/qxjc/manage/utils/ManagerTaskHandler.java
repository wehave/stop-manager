package com.qxjc.manage.utils;


import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.qxjc.manage.dao.UserMapper;
import com.qxjc.manage.pojo.User;
import com.qxjc.manage.service.UserService;

/**  
 * 创建时间：2018年6月27日 下午4:10:47  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：ManagerTaskHandler.java  
 * 类说明：  
 */
public class ManagerTaskHandler implements TaskListener{
	
	/** serialVersionUID*/
	private static final long serialVersionUID = -8428958293205164522L;
	
	@Override
	public void notify(DelegateTask delegateTask) {
		UserService userService = (UserService) SpringUtil.getBean(UserService.class);
		User existUser = (User) SecurityUtils.getSubject().getPrincipal();
		User parentUser = userService.findUserById(existUser.getManagerId());
		//当前用户
		String name = parentUser.getUsername();
		delegateTask.setAssignee(name);
	}
	
}
