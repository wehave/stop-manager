package com.qxjc.manage.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.shiro.SecurityUtils;

import com.qxjc.manage.pojo.User;

/**
 * 主键生成类 Title: keyUtil Description: Company:
 * 
 * @author Mo Xiaobao
 * @date 2018年7月21日
 */
public class keyUtil {

	/**
	 * 生成唯一的主键 格式: 时间+随机数 Title: genUniqueKey Description:
	 * 
	 * @param @return
	 * @return String
	 */
	public static synchronized String genUniqueKeyAll() {
		Random random = new Random();
		Integer number = random.nextInt(900000) + 100000;
		return System.currentTimeMillis() + String.valueOf(number);
	}

	public static synchronized String genUniqueKey() {
		Random random = new Random();
		Integer number = random.nextInt(9000) + 1000;
		return String.valueOf(System.currentTimeMillis()).substring(9, 13) + String.valueOf(number);
	}

	public static synchronized String genUniqueKeyByUid() {
		User existUser = (User) SecurityUtils.getSubject().getPrincipal();
		Integer uid = existUser.getId();
		SimpleDateFormat sFormat = new SimpleDateFormat("YYYYMMDDhh");
		return sFormat.format(new Date()) + uid;
	}

}
