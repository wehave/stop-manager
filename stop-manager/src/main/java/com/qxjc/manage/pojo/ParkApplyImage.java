package com.qxjc.manage.pojo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ParkApplyImage {

	private String id;

	/** 营业执照 */
	private String yyzz;

	/** 税务登记证 */
	private String swdjz;

	/** 法人身份证 */
	private String frsfzj;

	/** 经办人身份证 */
	private String jbrsfzj;

	/** 土地证 */
	private String tdz;

	/** 土地租赁合同1 */
	private String tdzlht1;

	/** 土地租赁合同2 */
	private String tdzlht2;

	/** 土地租赁合同3 */
	private String tdzlht3;

	/** 平面图 */
	private String pmt;

	/** 设施设备清单 */
	private String sbqd;

	/** 收费标准 */
	private String sfbz;

	/** 停放制度 */
	private String tfzd;

	/** 安全制度 */
	private String aqzd;

	/** 防汛制度 */
	private String fxzd;

	/** 消防管理制度 */
	private String xfzd;
	
	/** 智慧信息平台  */
	private String zhpt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getYyzz() {
		return yyzz;
	}

	public void setYyzz(String yyzz) {
		this.yyzz = yyzz == null ? null : yyzz.trim();
	}

	
	public String getSwdjz() {
		return swdjz;
	}

	public void setSwdjz(String swdjz) {
		this.swdjz = swdjz == null ? null : swdjz.trim();
	}
	
	public String getFrsfzj() {
		return frsfzj;
	}

	public void setFrsfzj(String frsfzj) {
		this.frsfzj = frsfzj == null ? null : frsfzj.trim();
	}

	public String getJbrsfzj() {
		return jbrsfzj;
	}

	public void setJbrsfzj(String jbrsfzj) {
		this.jbrsfzj = jbrsfzj == null ? null : jbrsfzj.trim();
	}

	public String getTdz() {
		return tdz;
	}

	public void setTdz(String tdz) {
		this.tdz = tdz == null ? null : tdz.trim();
	}

	public String getPmt() {
		return pmt;
	}

	public void setPmt(String pmt) {
		this.pmt = pmt == null ? null : pmt.trim();
	}

	public String getSbqd() {
		return sbqd;
	}

	public void setSbqd(String sbqd) {
		this.sbqd = sbqd == null ? null : sbqd.trim();
	}

	public String getSfbz() {
		return sfbz;
	}

	public void setSfbz(String sfbz) {
		this.sfbz = sfbz == null ? null : sfbz.trim();
	}

	public String getTfzd() {
		return tfzd;
	}

	public void setTfzd(String tfzd) {
		this.tfzd = tfzd == null ? null : tfzd.trim();
	}

	public String getAqzd() {
		return aqzd;
	}

	public void setAqzd(String aqzd) {
		this.aqzd = aqzd == null ? null : aqzd.trim();
	}

	public String getFxzd() {
		return fxzd;
	}

	public void setFxzd(String fxzd) {
		this.fxzd = fxzd == null ? null : fxzd.trim();
	}

	public String getXfzd() {
		return xfzd;
	}

	public void setXfzd(String xfzd) {
		this.xfzd = xfzd == null ? null : xfzd.trim();
	}

	public String getTdzlht1() {
		return tdzlht1;
	}

	public void setTdzlht1(String tdzlht1) {
		this.tdzlht1 = tdzlht1 == null ? null : tdzlht1.trim();
	}

	public String getTdzlht2() {
		return tdzlht2;
	}

	public void setTdzlht2(String tdzlht2) {
		this.tdzlht2 = tdzlht2 == null ? null : tdzlht2.trim();
	}

	public String getTdzlht3() {
		return tdzlht3;
	}

	public void setTdzlht3(String tdzlht3) {
		this.tdzlht3 = tdzlht3 == null ? null : tdzlht3.trim();
	}
	
	public String getZhpt() {
		return zhpt;
	}

	public void setZhpt(String zhpt) {
		this.zhpt = zhpt == null ? null : zhpt.trim();
	}

}
