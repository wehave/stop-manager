package com.qxjc.manage.pojo;

import java.util.Date;

import lombok.ToString;


@ToString
public class ParkApplyInfo {
	
 
	private String id;

    /** 图像id */
    private String applyImgId;
    
    /** 用户id */
    private String userId;

    /** 部门id  */
    private String deptId;

    /** 业务类型 1:新增   2:年审   */
    private Integer applyType;

    /** 名称 */
    private String name;

    /** 地址 */
    private String address;

    /** 负责人 */
    private String header;

    /** 负责人电话 */
    private String headerPhone;

    /** 法人 */
    private String boss;

    /** 法人电话 */
    private String bossPhone;

    /** 停车场类型 1:公用 2:私用 */
    private Integer type;

    /** 总数 */
    private Integer parkSum;

    /** 总面积 */
    private Float parkArea;

    /** 地下 */
    private Integer underground = 0;

    /** 地上 */
    private Integer ground = 0;

    /** 院内 */
    private Integer yard = 0;

    /** 机械 */
    private Integer machine = 0;

    /** 创建时间 */
    private Date createTime;

    /** 备注 */
    private String remark;
    
    /** 状态 0:开始 1:审核 2:完成 3:删除  */
    private Integer state;
    
    /** 修改状态 0:可修改 1:不可修改  */
    private Integer  modified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getApplyImgId() {
        return applyImgId;
    }

    public void setApplyImgId(String applyImgId) {
        this.applyImgId = applyImgId == null ? null : applyImgId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
    
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }

    public Integer getApplyType() {
        return applyType;
    }

    public void setApplyType(Integer applyType) {
        this.applyType = applyType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header == null ? null : header.trim();
    }

    public String getHeaderPhone() {
        return headerPhone;
    }

    public void setHeaderPhone(String headerPhone) {
        this.headerPhone = headerPhone == null ? null : headerPhone.trim();
    }

    public String getBoss() {
        return boss;
    }

    public void setBoss(String boss) {
        this.boss = boss == null ? null : boss.trim();
    }

    public String getBossPhone() {
        return bossPhone;
    }

    public void setBossPhone(String bossPhone) {
        this.bossPhone = bossPhone == null ? null : bossPhone.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getParkSum() {
        return parkSum;
    }

    public void setParkSum(Integer parkSum) {
        this.parkSum = parkSum;
    }

    public Float getParkArea() {
        return parkArea;
    }

    public void setParkArea(Float parkArea) {
        this.parkArea = parkArea;
    }

    public Integer getUnderground() {
        return underground;
    }

    public void setUnderground(Integer underground) {
        this.underground = underground;
    }

    public Integer getGround() {
        return ground;
    }

    public void setGround(Integer ground) {
        this.ground = ground;
    }

    public Integer getYard() {
        return yard;
    }

    public void setYard(Integer yard) {
        this.yard = yard;
    }

    public Integer getMachine() {
        return machine;
    }

    public void setMachine(Integer machine) {
        this.machine = machine;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
    
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
    
 	public Integer getModified() {
 		return modified;
 	}

 	public void setModified(Integer modified) {
 		this.modified = modified;
 	}

    
}