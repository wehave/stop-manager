package com.qxjc.manage.common;

public interface StatusCode {

	int STATUS_NEW = 0;

	int STATUS_AUDIT = 1;
	
	int STATUS_FINISHED = 2;
	
	int STATUS_DELTETED = 3;

}
