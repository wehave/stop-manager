package com.qxjc.manage.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;

import com.qxjc.manage.entity.WorkflowParam;
import com.qxjc.manage.pojo.ParkApplyInfo;

public interface WorkflowService {
	
	void saveNewDeploye(File file,String filename);

	List<Deployment> findDeploymentList();

	List<ProcessDefinition> findProcessDefinitionList();

	void deleteProcessDefinitionByDeploymentId(String deploymentId);

	InputStream findImageInputStream(String deploymentId, String imageName);

	void startProcess(String id,String username);

	List<Task> findTaskListByName(String name);

	String findTaskFormKeyByTaskId(String taskId);

	ParkApplyInfo findApplyInfoByTaskId(String taskId);

	List<Comment> findCommentByTaskId(String taskId);

	List<String> findOutComeListByTaskId(String taskId);

	void saveSubmitTask(WorkflowParam workflowParam,String username);

	ProcessDefinition findProcessDefinitionByTaskId(String taskId);

	Map<String, Object> findCoordingByTask(String taskId);

	List<Comment> findCommentByApplyInfoId(String applyInfoId);

}
