package com.qxjc.manage.service;

import com.qxjc.manage.entity.UserDTO;
import com.qxjc.manage.entity.UserRolesVO;
import com.qxjc.manage.entity.UserSearchDTO;
import com.qxjc.manage.pojo.User;
import com.qxjc.manage.utils.PageDataResult;

/**
 * 
 *<p> @ClassName: UserService</p>
 *<p> @Description: TODO</p>
 *<p> @author litao</p>
 *<p> @date 2018年9月11日</p>
 *
 *<p> </p>
 */
public interface UserService {
	
	
	/**分页查询用户列表*/
	PageDataResult getUsers(UserSearchDTO userSearch, int page, int limit);

	/** 设置用户【新增或更新】*/
	String setUser(User user, String roleIds);

	/** 设置用户是否离职 */
	String setJobUser(Integer id, Integer isJob, Integer insertUid,
			Integer version);

	/**  删除用户 */
	String setDelUser(Integer id, Integer isDel, Integer insertUid,
			Integer version);

	/** 查询用户数据  */
	UserRolesVO getUserAndRoles(Integer id);

	/** 发送短信验证码*/
	String sendMsg(UserDTO user);

	/** 根据手机号查询用户数据  */
	User findUserByMobile(String mobile);

	/**  根据手机号发送短信验证码   */
	String sendMessage(int userId, String mobile);

	/**  修改用户手机号 */
	int updatePwd(Integer id, String password);

	/** 锁定用户  0:解锁；1：锁定  */
	int setUserLockNum(Integer id, int isLock);
	
	User findUserById(Integer id);
}
