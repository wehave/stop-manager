package com.qxjc.manage.service;


import java.util.List;

import com.qxjc.manage.pojo.ParkApplyImage;
import com.qxjc.manage.pojo.ParkApplyInfo;


/**
 * 
 *<p> @ClassName: ParkService</p>
 *<p> @Description: TODO</p>
 *<p> @author litao</p>
 *<p> @date 2018年12月11日</p>
 *
 *<p> </p>
 */
public interface ParkService {
	
	int  insertParkApplyImage(ParkApplyImage parkApplyImage);
	
	int  insertParkApplyInfo(ParkApplyInfo parkApplyInfo);
	
	int updateParkApplyInfo(ParkApplyInfo parkApplyInfo);
	
	int updateParkApplyImage(ParkApplyImage parkApplyImage);
	
	List<ParkApplyInfo> selectParkApplyInfoByUserId(String userId);
	
	ParkApplyImage selectParkApplyImageById(String id);
	
	ParkApplyInfo selectParkApplyInfoById(String id);

}
