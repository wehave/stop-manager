package com.qxjc.manage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qxjc.manage.dao.DeptMapper;
import com.qxjc.manage.pojo.Dept;

@Service
public class DeptServiceImpl implements DeptService{
	
	@Autowired
	private DeptMapper deptMapper;

	@Override
	public List<Dept> DeptList() {
		return deptMapper.selectAll();
	}

}
