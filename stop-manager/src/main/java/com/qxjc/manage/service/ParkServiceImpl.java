package com.qxjc.manage.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qxjc.manage.dao.ParkApplyImageMapper;
import com.qxjc.manage.dao.ParkApplyInfoMapper;
import com.qxjc.manage.pojo.ParkApplyImage;
import com.qxjc.manage.pojo.ParkApplyInfo;

@Service
public class ParkServiceImpl implements ParkService {
	
	@Autowired
	private ParkApplyImageMapper applyImageMapper;
	
	@Autowired
	private ParkApplyInfoMapper applyInfoMapper;

	@Override
	@Transactional
	public int  insertParkApplyImage(ParkApplyImage parkApplyImage) {
		return applyImageMapper.insertSelective(parkApplyImage);
	}


	@Override
	public int insertParkApplyInfo(ParkApplyInfo parkApplyInfo) {
		return applyInfoMapper.insertSelective(parkApplyInfo);
	}


	@Override
	public ParkApplyInfo selectParkApplyInfoById(String id) {
		return applyInfoMapper.selectByPrimaryKey(id);
	}


	@Override
	public int updateParkApplyInfo(ParkApplyInfo parkApplyInfo) {
		return applyInfoMapper.updateByPrimaryKeySelective(parkApplyInfo);
	}


	@Override
	public List<ParkApplyInfo> selectParkApplyInfoByUserId(String userId) {
		return applyInfoMapper.selectByUserId(userId);
	}


	@Override
	public ParkApplyImage selectParkApplyImageById(String id) {
		return applyImageMapper.selectByPrimaryKey(id);
	}


	@Override
	public int updateParkApplyImage(ParkApplyImage parkApplyImage) {
		return applyImageMapper.updateByPrimaryKeySelective(parkApplyImage);
	}

}
