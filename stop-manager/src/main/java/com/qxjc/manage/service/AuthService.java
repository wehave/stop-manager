package com.qxjc.manage.service;

import com.qxjc.manage.entity.PermissionVO;
import com.qxjc.manage.entity.RoleVO;
import com.qxjc.manage.pojo.Permission;
import com.qxjc.manage.pojo.Role;

import java.util.List;

/**
 * 
 *<p> @ClassName: AuthService</p>
 *<p>@Description: </p>
 *<p>@author litao</p>
 *<p>@date 2018年9月11日</p>
 */
public interface AuthService {
	int addPermission(Permission permission);

	List<Permission> permList();

	int updatePerm(Permission permission);

	Permission getPermission(int id);

	String delPermission(int id);

	/** 查询所有角色 */
	List<Role> roleList();

	/** 关联查询权限树列表 */
	List<PermissionVO> findPerms();

	/** 添加角色 */
	String addRole(Role role, String permIds);

	RoleVO findRoleAndPerms(Integer id);

	/** 更新角色并授权 */
	String updateRole(Role role, String permIds);

	/** 删除角色以及它对应的权限 */
	String delRole(int id);

	/** 查找所有角色 */
	List<Role> getRoles();

	/** 根据用户获取角色列表 */
	List<Role> getRoleByUser(Integer userId);

	/** 根据角色id获取权限数据 */
	List<Permission> findPermsByRoleId(Integer id);

	/** 根据用户id获取权限数据 */
	List<PermissionVO> getUserPerms(Integer id);
}
